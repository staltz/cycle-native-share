import xs, {Stream} from 'xstream';
import {Share} from 'react-native';

export type SharedContent = {
  id?: number;
  message: string;
  title: string;
  dialogTitle?: string;
};

export function shareDriver(sink: Stream<SharedContent>): Stream<number> {
  const response$ = xs.create<number>();
  sink.addListener({
    next: obj => {
      const id = obj.id;
      const promise = Share.share(
        {message: obj.message, title: obj.title},
        {dialogTitle: obj.dialogTitle},
      );
      if (typeof id === 'number') {
        promise.then(() => response$._n(id));
      }
    },
  });
  return response$;
}
