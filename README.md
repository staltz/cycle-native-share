# Cycle Native Share

**A Cycle.js Driver for interacting with React Native's [Share](http://facebook.github.io/react-native/docs/0.55/share) API**

```
npm install cycle-native-share
```

## Usage

### Sink

Stream of a object that has fields:

```js
{
  message: /* REQUIRED string */,
  title: /* REQUIRED string */,

  id: /* OPTIONAL number */,
  dialogTitle: /* OPTIONAL string */,
}
```

### Source

Stream of ID numbers that indicate which sink requests have been executed (have been shared).

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license
